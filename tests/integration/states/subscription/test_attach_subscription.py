import copy
import time

import pytest


@pytest.mark.asyncio
async def test_attach_subscription_to_mg(
    hub,
    ctx,
    subscription_fixture,
    management_group_fixture,
):
    """
    This test attaches subscription to management group, describes all subscriptions
     and de-associates attached subscription.
    """
    # Attach subscription to management group
    subscription_props = subscription_fixture.get("properties")
    subscription_id = subscription_props["subscriptionId"]
    management_group_id = management_group_fixture.get("name")
    attach_subscription_name = "idem-test-attach-subscription-" + str(int(time.time()))
    attach_sub_mg_parameters = {
        "management_group_id": management_group_id,
        "subscription_id": subscription_id,
    }

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    # Create resource group with --test
    attach_sub_mg_ret = (
        await hub.states.azure.subscription.attach_subscriptions.present(
            test_ctx,
            name=attach_subscription_name,
            management_group_id=management_group_id,
            subscription_id=subscription_id,
        )
    )
    assert attach_sub_mg_ret["result"], attach_sub_mg_ret["comment"]
    assert not attach_sub_mg_ret["old_state"] and attach_sub_mg_ret["new_state"]
    assert (
        f"Would create azure.subscription.attach_subscriptions '{attach_subscription_name}'"
        in attach_sub_mg_ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=attach_sub_mg_ret["new_state"],
        expected_old_state=None,
        expected_new_state=attach_sub_mg_parameters,
        management_group_id=management_group_id,
        subscription_id=subscription_id,
        idem_resource_name=attach_subscription_name,
    )
    resource_id = attach_sub_mg_ret["new_state"].get("resource_id")
    assert (
        f"/providers/Microsoft.Management/managementGroups/{management_group_id}/subscriptions/{subscription_id}"
        == resource_id
    )

    # Attach Subscription in real
    attach_sub_mg_ret = (
        await hub.states.azure.subscription.attach_subscriptions.present(
            ctx,
            name=attach_subscription_name,
            **attach_sub_mg_parameters,
        )
    )
    assert (
        f"Created azure.subscription.attach_subscriptions '{attach_subscription_name}'"
        in attach_sub_mg_ret["comment"]
    )
    assert attach_sub_mg_ret["result"], attach_sub_mg_ret["comment"]
    assert not attach_sub_mg_ret["old_state"] and attach_sub_mg_ret["new_state"]
    check_returned_states(
        old_state=None,
        new_state=attach_sub_mg_ret["new_state"],
        expected_old_state=None,
        expected_new_state=attach_sub_mg_parameters,
        subscription_id=subscription_id,
        management_group_id=management_group_id,
        idem_resource_name=attach_subscription_name,
    )
    resource_id = attach_sub_mg_ret["new_state"].get("resource_id")
    assert (
        f"/providers/Microsoft.Management/managementGroups/{management_group_id}/subscriptions/{subscription_id}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"https://management.azure.com{resource_id}?api-version=2020-05-01",
        retry_count=5,
        retry_period=10,
    )

    attach_sub_mg_update_parameters = {
        "management_group_id": management_group_id,
        "subscription_id": subscription_id,
    }
    # Update resource group with --test
    attach_sub_mg_ret = (
        await hub.states.azure.subscription.attach_subscriptions.present(
            test_ctx, name=attach_subscription_name, **attach_sub_mg_update_parameters
        )
    )
    assert attach_sub_mg_ret["result"], attach_sub_mg_ret["comment"]
    assert attach_sub_mg_ret["old_state"] and attach_sub_mg_ret["new_state"]
    assert (
        f"No update required azure.subscription.attach_subscriptions '{attach_subscription_name}'"
        in attach_sub_mg_ret["comment"]
    )
    check_returned_states(
        old_state=attach_sub_mg_ret["old_state"],
        new_state=attach_sub_mg_ret["new_state"],
        expected_old_state=attach_sub_mg_parameters,
        expected_new_state=attach_sub_mg_update_parameters,
        management_group_id=management_group_id,
        subscription_id=subscription_id,
        idem_resource_name=attach_subscription_name,
    )
    resource_id = attach_sub_mg_ret["new_state"].get("resource_id")
    assert (
        f"/providers/Microsoft.Management/managementGroups/{management_group_id}/subscriptions/{subscription_id}"
        == resource_id
    )

    # Update attach subscription in real
    attach_sub_mg_ret = (
        await hub.states.azure.subscription.attach_subscriptions.present(
            ctx,
            name=attach_subscription_name,
            **attach_sub_mg_update_parameters,
        )
    )
    assert attach_sub_mg_ret["result"], attach_sub_mg_ret["comment"]
    assert attach_sub_mg_ret["old_state"] and attach_sub_mg_ret["new_state"]
    assert (
        f"No update required azure.subscription.attach_subscriptions '{attach_subscription_name}'"
        in attach_sub_mg_ret["comment"]
    )
    check_returned_states(
        old_state=attach_sub_mg_ret["old_state"],
        new_state=attach_sub_mg_ret["new_state"],
        expected_old_state=attach_sub_mg_parameters,
        expected_new_state=attach_sub_mg_parameters,
        management_group_id=management_group_id,
        subscription_id=subscription_id,
        idem_resource_name=attach_subscription_name,
    )
    resource_id = attach_sub_mg_ret["new_state"].get("resource_id")
    assert (
        f"/providers/Microsoft.Management/managementGroups/{management_group_id}/subscriptions/{subscription_id}"
        == resource_id
    )

    # Delete attached subscription from  management group with --test
    attach_sub_mg_del_ret = (
        await hub.states.azure.subscription.attach_subscriptions.absent(
            test_ctx,
            name=attach_subscription_name,
            **attach_sub_mg_parameters,
        )
    )
    assert attach_sub_mg_del_ret["result"], attach_sub_mg_del_ret["comment"]
    assert attach_sub_mg_del_ret["old_state"] and not attach_sub_mg_del_ret["new_state"]
    assert (
        f"Would de-associates azure.subscription.attach_subscriptions '{attach_subscription_name}'"
        in attach_sub_mg_del_ret["comment"]
    )
    check_returned_states(
        old_state=attach_sub_mg_del_ret["old_state"],
        new_state=None,
        expected_old_state=attach_sub_mg_parameters,
        expected_new_state=None,
        management_group_id=management_group_id,
        subscription_id=subscription_id,
        idem_resource_name=attach_subscription_name,
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"https://management.azure.com{resource_id}?api-version=2020-05-01",
        retry_count=5,
        retry_period=10,
    )

    # time.sleep(100)
    # De associate attached subscription from management group
    attach_sub_mg_del_ret = (
        await hub.states.azure.subscription.attach_subscriptions.absent(
            ctx,
            name=attach_subscription_name,
            management_group_id="78587530-29a0-4cb3-9080-4a3296cb3fdf",
            subscription_id="d9b5e642-5725-44d7-a3ac-03dc467be5f6",
        )
    )
    assert attach_sub_mg_del_ret["result"], attach_sub_mg_del_ret["comment"]
    assert attach_sub_mg_del_ret["old_state"] and not attach_sub_mg_del_ret["new_state"]
    assert (
        f"De-associated azure.subscription.attach_subscriptions '{attach_subscription_name}'"
        in attach_sub_mg_del_ret["comment"]
    )

    check_returned_states(
        old_state=attach_sub_mg_del_ret["old_state"],
        new_state=None,
        expected_old_state=attach_sub_mg_parameters,
        expected_new_state=None,
        management_group_id="78587530-29a0-4cb3-9080-4a3296cb3fdf",
        subscription_id="d9b5e642-5725-44d7-a3ac-03dc467be5f6",
        idem_resource_name=attach_subscription_name,
    )

    # De-associate subscription again from management group
    attach_sub_mg_del_ret = (
        await hub.states.azure.subscription.attach_subscriptions.absent(
            ctx,
            name=attach_subscription_name,
            management_group_id="78587530-29a0-4cb3-9080-4a3296cb3fdf",
            subscription_id="d9b5e642-5725-44d7-a3ac-03dc467be5f6",
        )
    )
    assert attach_sub_mg_del_ret["result"], attach_sub_mg_del_ret["comment"]
    assert (
        not attach_sub_mg_del_ret["old_state"]
        and not attach_sub_mg_del_ret["new_state"]
    )
    assert (
        f"azure.subscription.attach_subscriptions '{attach_subscription_name}' is not associated with management group"
        in attach_sub_mg_del_ret["comment"]
    )
    attach_sub_mg_ret = (
        await hub.states.azure.subscription.attach_subscriptions.present(
            ctx,
            name=attach_subscription_name,
            management_group_id="78587530-29a0-4cb3-9080-4a3296cb3fdf",
            subscription_id="d9b5e642-5725-44d7-a3ac-03dc467be5f6",
        )
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    management_group_id,
    subscription_id,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert management_group_id == old_state.get("management_group_id")
        assert subscription_id == old_state.get("subscription_id")
    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert management_group_id == new_state.get("management_group_id")
        assert subscription_id == new_state.get("subscription_id")
